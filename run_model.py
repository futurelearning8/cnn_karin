from tensorflow.keras.datasets import cifar10
from tensorflow import keras
import numpy as np
from sklearn.metrics import confusion_matrix
from utils import print_confusion_matrix


def main():
    # data
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    # Normalize pixel values to be between 0 and 1
    x_train, x_test = x_train / 255.0, x_test / 255.0
    class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',
                   'dog', 'frog', 'horse', 'ship', 'truck']

    # model
    model = keras.models.load_model('models/model.185.h5')

    results = model.evaluate(x_test, y_test)
    print("test loss, test acc:", results)

    # make prediction.
    pred = model.predict(x_test)
    # Convert predictions classes to one hot vectors
    Y_pred_classes = np.argmax(pred, axis=1)
    # Convert validation observations to one hot vectors
    Y_true = y_test.flatten()
    cf_matrix = confusion_matrix(Y_true, Y_pred_classes)
    print(cf_matrix)
    print_confusion_matrix(cf_matrix, class_names)


if __name__ == "__main__":
    main()
