from tensorflow.keras.datasets import cifar10
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf
import numpy as np
from sklearn.metrics import confusion_matrix
import datetime
from tensorflow.keras.regularizers import l2
from utils import print_confusion_matrix


def block_layers(components, f, x):
    for i in range(components):
        x = layers.Conv2D(filters=f, kernel_size=3, padding="same", activation="relu", kernel_regularizer=l2(0.001))(x)
    x = layers.MaxPool2D(pool_size=(2, 2))(x)
    x = layers.Dropout(0.2)(x)
    return x


def build_model():
    inputs = keras.Input(shape=(32, 32, 3))
    x = block_layers(2, 32, inputs)
    x = block_layers(2, 64, x)
    x = block_layers(2, 128, x)
    # x = block_layers(2, 512, x)
    # x = block_layers(2, 512, x)
    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Flatten()(x)
    # x = layers.Dense(units=256, activation="relu")(x)
    x = layers.Dropout(0.2)(x)
    x = layers.Dense(units=128, activation="relu", kernel_regularizer=l2(0.001))(x)
    x = layers.Dropout(0.2)(x)
    x = layers.Dense(units=10, activation="relu", kernel_regularizer=l2(0.001))(x)
    outputs = layers.Softmax()(x)
    model = keras.Model(inputs=inputs, outputs=outputs)
    return model


def main():
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    # Normalize pixel values to be between 0 and 1
    x_train, x_test = x_train / 255.0, x_test / 255.0

    epochs = 200
    batch_size = 64

    class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',
                   'dog', 'frog', 'horse', 'ship', 'truck']

    model = build_model()
    print(model.summary())
    tf.keras.utils.plot_model(model, to_file="model.png", show_shapes=True)
    loss = keras.losses.SparseCategoricalCrossentropy()
    opt = keras.optimizers.SGD(lr=0.01, momentum=0.9)
    model.compile(optimizer=opt, loss=loss, metrics=['accuracy'])

    checkpoint_callback = tf.keras.callbacks.ModelCheckpoint('models/model.{epoch:02d}.h5', monitor='val_accuracy', verbose=1,
                                                             save_best_only=True, mode='max')
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    reduce_lr_callback = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.8, patience=8, min_lr=0.001)
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    callbacks_list = [checkpoint_callback, reduce_lr_callback, tensorboard_callback]

    model.fit(x=x_train, y=y_train, epochs=epochs, batch_size=batch_size, callbacks=callbacks_list,
              validation_split=0.1)

    results = model.evaluate(x_test, y_test)
    print("test loss, test acc:", results)

    # make prediction.
    pred = model.predict(x_test)
    Y_pred_classes = np.argmax(pred, axis=1)

    # Convert validation observations to one hot vectors
    Y_true = y_test.flatten()
    cf_matrix = confusion_matrix(Y_true, Y_pred_classes)
    print(cf_matrix)
    print_confusion_matrix(cf_matrix, class_names)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
